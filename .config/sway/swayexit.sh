#!/bin/bash
choice=$(echo -e "no\nyes" | wmenu -i -p "Do you want to quit sway?")

if [ $choice = yes ]
then
	swaymsg exit
fi
