#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias pakupd='run0 pacman -Syu'
alias pakrm='run0 pacman -Rcsn'
alias pakclean='run0 sh -c "pacman -Scc && pacman -Qqd | pacman -Rsun -"'
alias q='exit'

fastfetch
