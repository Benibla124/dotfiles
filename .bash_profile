#
# ~/.bash_profile
#
export EDITOR=vim
export OPENER=xdg-open
export PATH=~/.local/bin:$PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
	export WLR_RENDERER=vulkan
	export ELECTRON_OZONE_PLATFORM_HINT=auto
	exec sway
fi
